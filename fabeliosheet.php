<?php
require __DIR__ . '/vendor/autoload.php';

class dbMysql{
    private $conn;
        // connection to database
        public function connect() {
            define("DB_HOST", "localhost");
            define("DB_USER", "root");
            define("DB_PASSWORD", "root"); // change your password database
            define("DB_DATABASE", "fabelio"); 
             
            // connection to mysql database
            $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
            // return database handler
            return $this->conn;
        }
    }

class mainData{
    public function sheetData(){
        $db = new dbMysql();
        $conn = $db->connect();
        
        $stmt = $conn->prepare("SELECT * FROM vw_fabelio");
        $i = 0;
        
        //===============Inisial otentication=====================
        $client = new \Google_Client();
        $client->setApplicationName('My PHP App');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');

        //==============proses input file json================
        $jsonAuth = getenv('JSON_AUTH');
        $client->setAuthConfig(__DIR__ . '/Quickstart-4cfe156e3a87.json');
        $sheets = new \Google_Service_Sheets($client);

        $data = [];

        $spreadsheetId = '1FjyU4xmrasIt5R7EDSAYMaEfIvoaIwJ9zqT0H9yAM_s'; // change spreadsheetId 
        $range = 'Sheet1!A3:B'; 
        $rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);

        if (isset($rows['values'])) {
            
                //============Input Data===============
                if ($stmt->execute()) {
                    $DataQ = $stmt->get_result();
                    
                    while ($list = $DataQ->fetch_array())
                                    {
                                        $currentRow = 2+$i; 
                                        $updateRange = 'A'.$currentRow; 
                                        $updateBody = new \Google_Service_Sheets_ValueRange([
                                            'range' => $updateRange,
                                            'majorDimension' => 'ROWS',
                                            'values' => ['values' => $list['ticket_created']],
                                        ]); 
                                            $sheets->spreadsheets_values->update(
                                            $spreadsheetId,
                                            $updateRange,
                                            $updateBody,
                                            ['valueInputOption' => 'USER_ENTERED']
                                        );
                                        
                                        $currentRow1 = 2+$i; 
                                        $updateRange1 = 'B'.$currentRow1; 
                                        $updateBody1 = new \Google_Service_Sheets_ValueRange([
                                            'range' => $updateRange1,
                                            'majorDimension' => 'ROWS',
                                            'values' => ['values' => $list['average']],
                                        ]); 
                                            $sheets->spreadsheets_values->update(
                                            $spreadsheetId,
                                            $updateRange1,
                                            $updateBody1,
                                            ['valueInputOption' => 'USER_ENTERED']
                                        );
                                        
                                        $i++;
                                    }
                                    
                    $stmt->close();
                    
                } 
                

                
        }
        $conn->close();
        return 'Done';
    }
}

$dt = new mainData();
$hsl = $dt->sheetData();
echo $hsl;
     