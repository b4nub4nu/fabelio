CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `fabelio`.`vw_fabelio` AS
select
    date_format(`x`.`ticket_created_at`, '%Y-%m-%d') AS `ticket_created`,
    left(sec_to_time(avg(time_to_sec(`x`.`selisih`))),
    8) AS `average`
from
    (
    select
        `fabelio`.`tbconversation`.`ticket_created_at` AS `ticket_created_at`,
        `fabelio`.`tbconversation`.`ticket_id` AS `ticket_id`,
        timediff(min(`fabelio`.`tbconversation`.`created_at`),
        `fabelio`.`tbconversation`.`ticket_created_at`) AS `selisih`
    from
        `fabelio`.`tbconversation`
    where
        `fabelio`.`tbconversation`.`author_id` = 924212557
    group by
        `fabelio`.`tbconversation`.`ticket_id`,
        `fabelio`.`tbconversation`.`ticket_created_at`
    order by
        `fabelio`.`tbconversation`.`ticket_created_at`) `X`
group by
    date_format(`x`.`ticket_created_at`, '%Y-%m-%d')
order by
    `x`.`ticket_created_at`