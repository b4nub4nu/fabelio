<?php
//------main-------456//

class core{
   
    private $dataArray;
    private $indexToFilter;

    public function __construct($dataArray, $indexToFilter){
        $this->dataArray = $dataArray;
        $this->indexToFilter = $indexToFilter;
    }
    private function getUnique(){
        foreach($this->dataArray as $key =>$value){
            $id[$value[$this->indexToFilter]]=$key;
        }
        return array_keys(array_flip(array_unique($id,SORT_REGULAR)));
    }
    public function getFiltered(){
        $array = $this->getUnique();
        $i=0;
        foreach($array as $key =>$value){
            $newAr[$i]=$this->dataArray[$value];
            $i++;
        }
        return $newAr;
    }
	public function initSort1($a, $b){
		return 	strtotime($a['data_loaded']) > strtotime($b['data_loaded']);
    }
	public function initSort2($a, $b){
		return 	strtotime($a['data_loaded']) < strtotime($b['data_loaded']);
    }
    public function initSort3($a, $b){
		return 	strtotime($a['created_at']) > strtotime($b['created_at']);
	}
	public function getSort($x){
		$array = $this->dataArray;
		usort($array, array( $this, $x ));
		return $array;
	}

}

class dbMysql{
private $conn;
    // connection to database
    public function connect() {
        define("DB_HOST", "localhost");
        define("DB_USER", "root");
        define("DB_PASSWORD", "root"); // change your password database
        define("DB_DATABASE", "fabelio"); 
         
        // connection to mysql database
        $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        // return database handler
        return $this->conn;
    }
}

class convData{
    public function convertData(){
        $db = new dbMysql();
        $conn = $db->connect();
        $data = file_get_contents("conversation_data.json");
        //$data = file_get_contents("coba.json");

        $arrayTmp = new core(json_decode( $data, TRUE ),"");
        $supper = new core($arrayTmp->getSort('initSort1'),"ticket_id");
        $resultTmp = json_encode($supper->getFiltered() );

        $array = new core(json_decode($resultTmp, TRUE ),"");
        $result = json_encode($array->getSort('initSort2'));
        $tes = json_decode($result, TRUE );

        foreach($tes as $row => $val) {
            $data_loaded = $val["data_loaded"];
            $ticket_id = $val["ticket_id"];
            $ticket_created_at = $val["ticket_created_at"];
            
            $cvTmp1 = new core(json_decode($val["conversations"], TRUE)["conversations"],"");
            $cvTmp2 = json_encode($cvTmp1->getSort('initSort3'));  
            $cvTmp = json_decode($cvTmp2, TRUE);

            foreach($cvTmp as $rowx => $valx) {
                $conversation_id = $valx["conversation_id"];
                $author_id = $valx["author_id"];
                $body = $valx["body"];
                $created_at = $valx["created_at"];
                $via_channel = $valx["via_channel"];
                $stmt = $conn->prepare("INSERT INTO tbConversation(data_loaded,ticket_id,ticket_created_at,conversation_id,author_id,body,created_at,via_channel) VALUES(?, ?, ?, ?, ? ,? ,? ,?)");
                $stmt->bind_param("sisiisss",$data_loaded,$ticket_id,$ticket_created_at,$conversation_id,$author_id,$body,$created_at,$via_channel);
                $stmt->execute();
                $stmt->close();  
            }
        }
        
        $conn->close();
        return 'Done';
    }
}

$dt = new convData();
$hsl = $dt->convertData();
echo $hsl;